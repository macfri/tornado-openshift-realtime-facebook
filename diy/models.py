"""
import os

here = os.path.dirname(os.path.abspath(__file__))
os.environ['PYTHON_EGG_CACHE'] = os.path.join(here, '..', 'misc/virtenv/lib/python2.6/site-packages')
virtualenv = os.path.join(here, '..', 'misc/virtenv/bin/activate_this.py')
execfile(virtualenv, dict(__file__=virtualenv))
"""

import settings

from sqlalchemy import Column, String, Integer, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Dictionary(Base):
    __tablename__ = 'dictionary'

    id = Column(Integer, primary_key=True)
    title_en = Column(String(500), default='', nullable=False)
    title_es = Column(String(500), default='', nullable=False)


if __name__ == '__main__':



    engine = create_engine(settings.DATABASE_DNS)
    session = sessionmaker(engine)()
    
    #Base.metadata.create_all(engine)

    """
    dictionary = Dictionary()
    dictionary.title_en = 'polite'
    dictionary.title_es = 'cortes'
    session.add(dictionary)
    session.commit()
    """
    
    """
    queryset = session.query(Dictionary).all()

    for q in queryset:
        print q.id, q.title_en, q.title_es
    """

