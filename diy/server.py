#!/usr/bin/env python
import os

here = os.path.dirname(os.path.abspath(__file__))
os.environ['PYTHON_EGG_CACHE'] = os.path.join(
    here, '..', 'misc/virtenv/lib/python2.6/site-packages')

virtualenv = os.path.join(here, '..', 'misc/virtenv/bin/activate_this.py')
execfile(virtualenv, dict(__file__=virtualenv))

import settings
import logging

import tornado.ioloop

from tornado.web import RequestHandler, Application

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from models import Dictionary

import urllib


facebook_url = 'https://graph.facebook.com/'
callback_url = 'http://tornado-macfri.rhcloud.com/facebook'

app_id = '143551124125'
app_key = '230871743633613'
app_secret = 'f0013fd2162b433e0c6bf49e93b704c0'

#verify_token = app_secret
verify_token = 'abc'

real_time_url = '%s%s/subscriptions' % (facebook_url, app_id)

url_get_access_token = '%s/oauth/access_token?client_id=%s&client_secret=%s&grant_type=client_credentials' % (
    facebook_url, app_key, app_secret)

session = sessionmaker(create_engine(settings.DATABASE_DNS))()

templates = 'templates/dictionary'


class MainHandler(RequestHandler):
    def get(self):
        self.render('index.html')


class DictionaryClass(RequestHandler):
    def get(self):
        queryset = session.query(Dictionary).all()
        self.render('%s/%s' % (templates, 'list.html'), queryset=queryset)


class DictionaryAddClass(RequestHandler):
    def get(self, **kwargs):
        status_code = None

        if 'status_code' in kwargs:
            status_code = kwargs['status_code']

        self.render('%s/%s' % (templates, 'add.html'), status_code=status_code)

    def post(self):

        status_code = 0

        title_en = self.get_argument('title_en', '')
        title_es = self.get_argument('title_es', '')

        if len(title_en) < 1 or len(title_es) < 1:
            status_code = 1
        else:

            dictionary = Dictionary()
            dictionary.title_en = title_en
            dictionary.title_es = title_es
            session.add(dictionary)

            try:
                session.commit()
            except Exception as exc:
                logging.error(exc)
                status_code = 2
            else:
                self.redirect('/dictionary')
                return

        logging.error('status_code: %s' % status_code)

        self.get(status_code=status_code)


class Facebook(RequestHandler):

    def get(self):

        hub_mode = self.get_argument('hub.mode', None)
        hub_verify_token = self.get_argument('hub.verify_token', None)
        hub_challenge = self.get_argument('hub.challenge', None)

        logging.error('hub_mode: %s' % hub_mode)
        logging.error('hub_verify_token: %s' % hub_verify_token)
        logging.error('hub_challenge: %s' % hub_challenge)

        if hub_verify_token == verify_token and hub_mode == 'subscribe':
            logging.error('ok verification!')
            self.finish(hub_challenge)

    def post(self):

        body = self.get_argument('body', None)
        logging.error('body: %s' % body)

        access_token = urllib.urlopen(
            url_get_access_token).read().split('=')[1]
        logging.error('access_token: %s' % access_token)

        response = urllib.urlopen(
            '%s?access_token=%s' % (real_time_url, access_token)).read()
        logging.error(response)


class FacebookData(RequestHandler):

    def get(self):

        access_token = urllib.urlopen(
            url_get_access_token).read().split('=')[1]

        params = urllib.urlencode({
                'access_token': access_token,
                'object': 'page',
                'fields': 'feed, likes',
                'callback_url': callback_url + '?access_token=' + access_token,
                'verify_token': verify_token})

        self.finish(dict(url=real_time_url, params=params))


application = Application([
    (r"/", MainHandler),
    (r"/dictionary", DictionaryClass),
    (r"/dictionary/add", DictionaryAddClass),
    (r"/facebook/data", FacebookData),
    (r"/facebook", Facebook),
], debug=True)

if __name__ == "__main__":
    try:
        address = os.environ['OPENSHIFT_INTERNAL_IP']
    except:
        address = '127.0.0.1'

    logging.error(address)

    application.listen(8080, address)
    tornado.ioloop.IOLoop.instance().start()
